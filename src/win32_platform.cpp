/*
 * win32platform.cpp
 *
 *  Created on: 21 gru 2014
 *      Author: Miko Kuta
 */

#include "common.h"

#include <windows.h>
#include <Shlwapi.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <math.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <vector>

#define MAX_FILENAME_LENGTH 1024

struct win32_app_state
{
	bool32 isRunning;
	game_input input;
};

global_variable win32_app_state appState = {};

uint64 platformGetFileModifiedTime(const char* fileName)
{
	uint64 result = 0;
	WIN32_FILE_ATTRIBUTE_DATA fileAttributes;
	if (GetFileAttributesEx(fileName, GetFileExInfoStandard, &fileAttributes))
	{
		FILETIME lastWriteTime = fileAttributes.ftLastWriteTime;
		result = (uint64) (lastWriteTime.dwLowDateTime) | (uint64) (lastWriteTime.dwHighDateTime) << 32;
	}
	return result;
}

//local void win32ConcatStrings(const char* str1, const char* str2, char* result, uint32 resultbufferLength)
//{
//	size_t length1 = strlen(str1);
//	size_t length2 = strlen(str2);
//
//	Assert((length1 + length2) < resultbufferLength);
//
////	memset((void*)result,'x',resultbufferLength);
//	memcpy((void*) result, (const void*) str1, length1);
//	memcpy((void*) (result + length1), (const void*) str2, length2);
//	result[length1 + length2] = 0;
//}

local void win32ProcessButtonState(button_state *button, bool32 isDown)
{
	if (isDown != button->isDown)
	{
		button->switchCount++;
		button->isDown = isDown;
	}
}

//local LRESULT CALLBACK win32ProcessWindowMessages(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
//{
//	LRESULT result = 0;
//
//	switch (message)
//	{
//		case WM_DESTROY:
//			{
//
//		}
//		break;
//
//		case WM_CLOSE:
//			{
//			appState.isRunning = false;
//			PostQuitMessage(0);
//		}
//		break;
//
//		default:
//			{
//			result = DefWindowProc(window, message, wParam, lParam);
//		}
//	}
//	return result;
//}

#define KEY_IS_DOWN(lparam) ((lparam&0x40000000L)==0)
#define KEY_WAS_DOWN(lparam) ((lparam&0x80000000L)!=0)
#define PARAM_GET_HIGH_WORD(param) ((param>>16)&0xffff)
#define PARAM_GET_LOW_WORD(param) (param&0xffff)

local void win32ProcessPendingMessages(HWND window, win32_app_state* appState)
{
	MSG message = {};

	while (PeekMessage(&message, 0, 0, 0, PM_REMOVE))
	{
		switch (message.message)
		{
			case WM_QUIT:
				appState->isRunning = false;
			break;

			case WM_SYSKEYDOWN:
				case WM_SYSKEYUP:
				case WM_KEYDOWN:
				case WM_KEYUP:
				{
				uint32 vkCode = (uint32) message.wParam;

				bool32 wasDown = KEY_WAS_DOWN(message.lParam);
				bool32 isDown = KEY_IS_DOWN(message.lParam);
				0x10000000;

				if (wasDown != isDown)
				{
					if (message.wParam == VK_ESCAPE)
					{
						if (isDown)
						{
							appState->isRunning = false;
						}
					}
					else if (message.wParam == VK_UP)
					{
						win32ProcessButtonState(&appState->input.buttonUp, isDown);
					}
					else if (message.wParam == VK_DOWN)
					{
						win32ProcessButtonState(&appState->input.buttonDown, isDown);
					}
					else if (message.wParam == VK_LEFT)
					{
						win32ProcessButtonState(&appState->input.buttonLeft, isDown);
					}
					else if (message.wParam == VK_RIGHT)
					{
						win32ProcessButtonState(&appState->input.buttonRight, isDown);

					}
					else if (message.wParam == 'X')
					{
						win32ProcessButtonState(&appState->input.buttonJump, isDown);

					}
					else if (message.wParam == 'Z')
					{
						win32ProcessButtonState(&appState->input.buttonShoot, isDown);

					}
					else if (message.wParam == VK_F1)
					{
						win32ProcessButtonState(&appState->input.buttonDebug1, isDown);
					}
					else if (message.wParam == VK_F2)
					{
						win32ProcessButtonState(&appState->input.buttonDebug2, isDown);
					}
					else if (message.wParam == VK_F3)
					{
						win32ProcessButtonState(&appState->input.buttonDebug3, isDown);
					}
					else if (message.wParam == VK_F4)
					{
						win32ProcessButtonState(&appState->input.buttonDebug4, isDown);
					}
				}
			}
			break;

			case WM_LBUTTONUP:
				case WM_LBUTTONDOWN:
				{
				bool32 isDown = message.message == WM_LBUTTONDOWN;
				win32ProcessButtonState(&appState->input.buttonDebugMouse1, isDown);
			}
			break;

			case WM_RBUTTONUP:
				case WM_RBUTTONDOWN:
				{
				bool32 isDown = message.message == WM_RBUTTONDOWN;
				win32ProcessButtonState(&appState->input.buttonDebugMouse2, isDown);
			}
			break;

			case WM_MBUTTONUP:
				case WM_MBUTTONDOWN:
				{
				bool32 isDown = message.message == WM_MBUTTONDOWN;
				win32ProcessButtonState(&appState->input.buttonDebugMouse3, isDown);
			}
			break;

			case WM_MOUSEWHEEL:
				{
				real32 offset = GET_WHEEL_DELTA_WPARAM(message.wParam) / (real32) WHEEL_DELTA;
				appState->input.cursorWheelRel += offset;
			}
			break;

			default:
				{
				TranslateMessage(&message);
				DispatchMessage(&message);
			}
		}
	}
}

void printMemoryUsage()
{
	MEMORYSTATUSEX memoryStatus = {};
	memoryStatus.dwLength = sizeof(memoryStatus);

	BOOL memoryStatusResult = GlobalMemoryStatusEx(&memoryStatus);
	printf("\nMEMORY SPECS BEFORE GAME MEMORY ALLOC\n");
	if (memoryStatusResult)
	{
		printf("Total Physical Memory: %llu kb(%llu mb)\n", (uint64) (ToKilobytes(memoryStatus.ullAvailPhys)), (uint64) (ToMegabytes(memoryStatus.ullTotalPhys)));
		printf("Free Physical Memory: %llu kb(%llu mb)\n", (uint64) (ToKilobytes(memoryStatus.ullAvailPhys)), (uint64) (ToMegabytes(memoryStatus.ullAvailPhys)));
		printf("Total Virtual Memory: %llu kb(%llu mb)\n", (uint64) (ToKilobytes(memoryStatus.ullTotalVirtual)), (uint64) (ToMegabytes(memoryStatus.ullTotalVirtual)));
		printf("Free Virtual Memory: %llu kb(%llu mb)\n", (uint64) (ToKilobytes(memoryStatus.ullAvailVirtual)), (uint64) (ToMegabytes(memoryStatus.ullAvailVirtual)));
	}
	else
	{
		printf("Retrieving memory info failed, error: %d\n", GetLastError());
	}
}

int CALLBACK WinMain(HINSTANCE AppInstance, HINSTANCE PrevAppInstance, LPSTR commandLine, int showCommand)
{
	AllocConsole();
	// redirect unbuffered STDOUT to the console
	int outConsole;
	int inConsole;
	long stdOutHandle;
	long stdInHandle;
	FILE *outFile;
	FILE *inFile;
	stdOutHandle = (long) GetStdHandle(STD_OUTPUT_HANDLE);
	stdInHandle = (long) GetStdHandle(STD_INPUT_HANDLE);
	outConsole = _open_osfhandle(stdOutHandle, _O_TEXT);
	inConsole = _open_osfhandle(stdInHandle, _O_TEXT);

	outFile = _fdopen(outConsole, "w");
	*stdout = *outFile;
	setvbuf(stdout, NULL, _IONBF, 0);

	inFile = _fdopen(inConsole, "r");
	*stdin = *inFile;
	setvbuf(stdout, NULL, _IONBF, 0);

	HWND consoleWindow = GetConsoleWindow();
	SetWindowPos(consoleWindow, NULL, 0, 0, 0, 0, SWP_NOSIZE);

	appState.isRunning = true;

	LARGE_INTEGER prevTime = {};
	LARGE_INTEGER curTime = {};
	LARGE_INTEGER counterFrequency;
	QueryPerformanceCounter(&prevTime);
	QueryPerformanceFrequency(&counterFrequency);
	printf("Performance Counter frequency: %llu\n", (uint64) counterFrequency.QuadPart);
	uint32 targetFps = 60;
	uint64 frameDuration = (1000 / 60);
	real32 frameTimeDif = 1.0f / targetFps;

	uint32 framesElapsed = 0;
	LARGE_INTEGER frameCountStart = {};
	LARGE_INTEGER frameCountEnd = {};
	real64 frameDurationTotal = 0;

	char buffer[256];
	while (appState.isRunning)
	{
		for (int i = 0; i < INPUT_BUTTON_COUNT; i++)
		{
			appState.input.buttons[i].switchCount = 0;
		}
		appState.input.cursorWheelRel = 0;
		win32ProcessPendingMessages(consoleWindow, &appState);

		gets_s(buffer,256);
		printf(buffer);
		printf("\n");

		real64 duration = 0;
		do
		{
			QueryPerformanceCounter(&curTime);
			duration = (curTime.QuadPart - prevTime.QuadPart) * 1000 / (double) counterFrequency.QuadPart;
		}
		while (duration < frameDuration);
		prevTime = curTime;

	}

	return 0;
}
