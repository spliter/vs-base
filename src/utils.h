/*
 * game_utils.h
 *
 *  Created on: 31 mar 2015
 *      Author: Miko Kuta
 */

#ifndef GAME_UTILS_H_
#define GAME_UTILS_H_

#include "common.h"
#include "float.h"

#define RANDOM_MAX 32767

struct random_state
{
	uint32 seed;
	uint32 getUint32();
	int32 getInt32();
	int32 getInt32(int32 min, int32 max);
	real32 getReal32(real32 min, real32 max);
};

inline uint32 random_state::getUint32()
{
	seed = seed * 1103515245 + 12345;
	uint32 randRange = RANDOM_MAX + 1;
	return (uint32) (seed / (randRange * 2)) % randRange;
}

inline int32 random_state::getInt32() // 0 to RANDOM_MAX inclusive
{
	return (int32) getUint32();
}

inline int32 random_state::getInt32(int32 min, int32 max) //closed min, open max
{
	int32 result = min;
	int32 dif = max - min;
	if (dif != 0)
	{
		result = min + (getUint32()) % (dif);
	}
	return result;
}

inline real32 random_state::getReal32(real32 min, real32 max) //closed min, closed max
{
	real32 result;
	real32 dif = max - min;
	real64 range = (real64) getUint32() / (real64) RANDOM_MAX;
	result = (real32) (min + (dif) * range);
	return result;
}

#endif
