/*
 * platform_common.h
 *
 *  Created on: 3 kwi 2015
 *      Author: Miko Kuta
 */

#ifndef PLATFORM_COMMON_H_
#define PLATFORM_COMMON_H_
#include "types.h"

uint64 platformGetFileModifiedTime(const char* fileName);//returns 0 on failure

#endif
