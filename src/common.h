/*
 * game_common.h
 *
 *  Created on: 25 gru 2014
 *      Author: Miko Kuta
 */

#ifndef GAME_COMMON_H
#define GAME_COMMON_H
#include "types.h"

#define Assert(condition) if(!(condition)){*((int*)(0))=0;}

#define Kilobytes(n) ((n)*1024L)
#define Megabytes(n) (Kilobytes(n)*1024L)
#define Gigabytes(n) (Megabytes(n)*1024L)

#define ToKilobytes(n) ((n)/1024L)
#define ToMegabytes(n) (ToKilobytes(n)/1024L)
#define ToGigabytes(n) (ToMegabytes(n)/1024L)

#define STRINGIZEEX(value) #value
#define STRINGIZE(value) STRINGIZEEX(value)

#define MACRO_PROTECT(...) __VA_ARGS__

#define local static
#define global_variable static
#define persistent static

struct button_state
{
	int32 switchCount;
	bool32 isDown;

	inline bool32 wasPressed()
	{
		bool32 result = false;
		result = isDown && switchCount>0;
		return result;
	}

	inline bool32 isPressed()
	{
		bool32 result = false;
		result = isDown;
		return result;
	}

	inline bool32 wasReleased()
	{
		bool32 result = false;
		result = !isDown && switchCount>0;
		return result;
	}
};

#define INPUT_BUTTON_COUNT 13

struct game_input
{
	real32 cursorX,cursorY;
	real32 cursorWheelRel;

	union
	{
		//NOTE: update the INPUT_BUTTON_COUNT whenever you change the buttons in the struct
		button_state buttons[INPUT_BUTTON_COUNT];
		struct
		{
			button_state buttonUp;
			button_state buttonDown;
			button_state buttonLeft;
			button_state buttonRight;
			button_state buttonJump;
			button_state buttonShoot;

			button_state buttonDebug1;
			button_state buttonDebug2;
			button_state buttonDebug3;
			button_state buttonDebug4;

			button_state buttonDebugMouse1;
			button_state buttonDebugMouse2;
			button_state buttonDebugMouse3;
		};

	};
};

struct memory_manager
{
	uint8* memory;
	uint64 memorySize;
	uint64 used;
};

#define pushStruct(manager,struct) (struct*)pushMemory(manager,sizeof(struct))
#define pushStructArray(manager,struct, size) (struct*)pushMemory(manager,sizeof(struct)*size)

#ifndef NULL
#define NULL 0
#endif

uint8* pushMemory(memory_manager* manager,uint32 size)
{
	//TODO: make sure we're allocating it on a 4 byte aligned memory
	uint8* result = NULL;
	Assert(manager->used+size<manager->memorySize);
	result = manager->memory+manager->used;
	manager->used+=size;
	return result;
}


struct game_screen
{
	int32 width,height;
};
#endif
