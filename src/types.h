/*
 * types.h
 *
 *  Created on: 28 gru 2014
 *      Author: Miko Kuta
 */

#ifndef TYPES_H_
#define TYPES_H_

typedef signed char int8;
typedef signed short int16;
typedef signed int int32;
typedef __int64 int64;

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned __int64 uint64;

typedef int32 bool32;

typedef float real32;
typedef double real64;

#endif
