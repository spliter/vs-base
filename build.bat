rem @echo off
call setupEnv.bat
set CommonCompilerFlags=-MTd -nologo -Gm- -GR- -EHa- -Od -Oi -WX -W4 -wd4201 -wd4100 -wd4189 -FC -Z7
set Win32IncludeFlags= /I..\libs\include\
set Win32LinkerFlags= -incremental:no -opt:ref user32.lib gdi32.lib winmm.lib Shlwapi.lib

IF NOT EXIST build mkdir build
pushd build

REM 32-bit build
 del *.pdb > NUL 2> NUL
cl %CommonCompilerFlags%  /Fewin32_app.exe ..\src\win32_platform.cpp  %win32IncludeFlags% /link %Win32LinkerFlags%

popd

